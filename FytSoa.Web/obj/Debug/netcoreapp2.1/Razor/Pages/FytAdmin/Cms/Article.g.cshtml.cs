#pragma checksum "D:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Article.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f85606e75ee12069843018e7a130a2b1312511cc"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(FytSoa.Web.Pages.FytAdmin.Cms.Pages_FytAdmin_Cms_Article), @"mvc.1.0.razor-page", @"/Pages/FytAdmin/Cms/Article.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/FytAdmin/Cms/Article.cshtml", typeof(FytSoa.Web.Pages.FytAdmin.Cms.Pages_FytAdmin_Cms_Article), null)]
namespace FytSoa.Web.Pages.FytAdmin.Cms
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\_ViewImports.cshtml"
using FytSoa.Web;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f85606e75ee12069843018e7a130a2b1312511cc", @"/Pages/FytAdmin/Cms/Article.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"21c44af9864cf57cf01e8fd1fe389a8e352e148c", @"/Pages/_ViewImports.cshtml")]
    public class Pages_FytAdmin_Cms_Article : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "IsTop", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "IsHot", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "IsScroll", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "IsSlide", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "IsComment", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "IsWap", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "IsLink", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("layui-form form-cus"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString(""), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_10 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("padding:15px;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "D:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Article.cshtml"
  
    ViewData["Title"] = "文章管理";
    Layout = AdminLayout.Pjax(HttpContext);

#line default
#line hidden
            BeginContext(143, 363, true);
            WriteLiteral(@"<div id=""container"">
    <div class=""list-wall"">
        <div class=""layui-form list-search"">
            <div class=""layui-inline"">
                <input class=""layui-input"" id=""key"" autocomplete=""off"" placeholder=""请输入关键字查询"">
            </div>
            <div class=""layui-inline"">
                <select id=""attr"" lay-search="""">
                    ");
            EndContext();
            BeginContext(506, 28, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0e237cfdd1e04f04a290a59efcffdf92", async() => {
                BeginContext(523, 2, true);
                WriteLiteral("全部");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(534, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(556, 33, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4e6578af8ca7480aa003ff648dd6eb0f", async() => {
                BeginContext(578, 2, true);
                WriteLiteral("推荐");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(589, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(611, 33, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a0b3c5fe9dc4033b7e39dd9750309a5", async() => {
                BeginContext(633, 2, true);
                WriteLiteral("热点");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(644, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(666, 36, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "68b03edd31964792a02c1515104f9437", async() => {
                BeginContext(691, 2, true);
                WriteLiteral("滚动");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(702, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(724, 35, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "003cc342a8a346d09c8f83f98b6d6c78", async() => {
                BeginContext(748, 2, true);
                WriteLiteral("幻灯");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(759, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(781, 39, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f02c4e368dc140babad999d20a54db6a", async() => {
                BeginContext(807, 4, true);
                WriteLiteral("是否评论");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(820, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(842, 36, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1549808ef5d0497694ce9a16e197e8f2", async() => {
                BeginContext(864, 5, true);
                WriteLiteral("发布移动端");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(878, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(900, 36, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4f3b013a8bd04f4b87ffdb0451abb936", async() => {
                BeginContext(923, 4, true);
                WriteLiteral("转向链接");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_7.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(936, 556, true);
            WriteLiteral(@"
                </select>
            </div>
            <div class=""layui-inline"" style=""width:100px;"">
                <input type=""checkbox"" checked=""checked"" name=""close"" lay-filter=""switch"" lay-skin=""switch"" lay-text=""已审核|未审核"">
            </div>
            <button type=""button"" class=""layui-btn layui-btn-sm"" data-type=""toolSearch""><i class=""layui-icon layui-icon-search""></i> 搜索</button>
        </div>
        <table class=""layui-hide"" id=""tablist"" lay-filter=""tool""></table>
    </div>
    <div class=""copy-wall layui-hide"">
        ");
            EndContext();
            BeginContext(1492, 1212, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0c9869ef1c7347eeabfe0b346f254b04", async() => {
                BeginContext(1558, 320, true);
                WriteLiteral(@"
            <blockquote class=""layui-elem-quote"">请选择将要把数据转移/复制的指定栏目</blockquote>
            <div class=""layui-form-item"">
                <label class=""layui-form-label"">选择分类</label>
                <div class=""layui-input-block"">
                    <select name=""columnId"" lay-verify=""required"" lay-search="""">
");
                EndContext();
#line 39 "D:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Article.cshtml"
                          
                            if (Model.ColumnList.Any())
                            {
                                foreach (var item in Model.ColumnList)
                                {

#line default
#line hidden
                BeginContext(2101, 36, true);
                WriteLiteral("                                    ");
                EndContext();
                BeginContext(2137, 45, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "27b75b65050e4b748eb933d1600efec8", async() => {
                    BeginContext(2163, 10, false);
#line 44 "D:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Article.cshtml"
                                                        Write(item.Title);

#line default
#line hidden
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                BeginWriteTagHelperAttribute();
#line 44 "D:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Article.cshtml"
                                       WriteLiteral(item.Id);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2182, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 45 "D:\2018Project\飞易腾管理系统\SoaProJect\FytSoa.Web\Pages\FytAdmin\Cms\Article.cshtml"
                                }
                            }
                        

#line default
#line hidden
                BeginContext(2277, 420, true);
                WriteLiteral(@"                    </select>
                </div>
            </div>
            <div class=""layui-form-item"">
                <div class=""layui-input-block"">
                    <button class=""layui-btn"" lay-submit="""" lay-filter=""submit"">立即提交</button>
                    <button type=""button"" class=""layui-btn layui-btn-primary btn-open-close"">取消</button>
                </div>
            </div>
        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_10);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2704, 11530, true);
            WriteLiteral(@"
    </div>
    <script type=""text/html"" id=""toolbar"">
        <div class=""layui-btn-container"">
            <a href=""/fytadmin/cms/articlemodify"" class=""layui-btn layui-btn-sm""><i class=""layui-icon""></i> 新增</a>
            <button type=""button"" class=""layui-btn layui-btn-sm"" lay-event=""toolDel""><i class=""layui-icon""></i> 删除</button>
            <button type=""button"" class=""layui-btn layui-btn-sm"" lay-event=""toolRecycle""><i class=""layui-icon layui-icon-refresh-1""></i> 移动到回收站</button>
            <button type=""button"" class=""layui-btn layui-btn-sm"" lay-event=""toolCopy""><i class=""layui-icon layui-icon-templeate-1""></i> 复制</button>
            <button type=""button"" class=""layui-btn layui-btn-sm"" lay-event=""tooltransfer""><i class=""layui-icon layui-icon-senior""></i> 转移</button>
        </div>
    </script>
    <script type=""text/html"" id=""switchTpl"">
        <input type=""checkbox"" name=""status"" value=""{{d.id}}"" lay-skin=""switch"" disabled lay-text=""已审核|未审核"" lay-filter=""statusedit"" {{ d.audit==1?'check");
            WriteLiteral(@"ed':''}}>
    </script>
    <script>
        layui.config({
            base: '/themes/js/modules/'
        }).use(['table', 'layer', 'jquery', 'common', 'form'],
            function () {
                var table = layui.table,
                    layer = layui.layer,
                    $ = layui.jquery,
                    os = layui.common,
                    form = layui.form;
                form.render();
                table.render({
                    elem: '#tablist',
                    toolbar: '#toolbar',
                    url: '/api/article/getpages',
                    cols: [
                        [
                            { type: 'checkbox', fixed: 'left' },
                            {
                                field: 'title', title: '标题', fixed: 'left', templet: function (res) {
                                    return '<a href=""/fytadmin/cms/articlemodify?id=' + res.id + '"" class=""text-color"">' + res.title + '</a>';
                             ");
            WriteLiteral(@"   }
                            },
                            { field: 'tag', width: 200, title: '标签' },
                            {
                                field: 'iosVersion', width: 200, title: '属性', templet: function (res) {
                                    return attrHtml(res);
                                }
                            },
                            { field: 'audit', width: 120, title: '审核状态', templet: '#switchTpl' },
                            { field: 'sort', width: 100, title: '权重' },
                            { field: 'hits', width: 100, title: '点击量' },
                            { field: 'editDate', width: 200, title: '更新时间', sort: true }
                        ]
                    ],
                    page: true,
                    id: 'tables',
                    where: {
                        guid:'1'
                    }
                });

                var type=0,isCopy=0,selStr='',active = {
                    reload: f");
            WriteLiteral(@"unction () {
                        table.reload('tables',
                            {
                                page: {
                                    curr: 1
                                },
                                where: {
                                    guid: '1',
                                    where: $(""#attr"").val(),
                                    key: $('#key').val(),
                                    types:type
                                },
                                done: function(){
                                }
                            });
                    },
                    toolSearch: function () {
                        active.reload();
                    },
                    toolDel: function () {
                        var checkStatus = table.checkStatus('tables')
                            , data = checkStatus.data;
                        if (data.length === 0) {
                            os.error(");
            WriteLiteral(@"""请选择要删除的项目~"");
                            return;
                        }
                        var str = '';
                        $.each(data, function (i, item) {
                            str += item.id + "","";
                        });
                        layer.confirm('确定要执行批量删除吗？', function (index) {
                            layer.close(index);
                            var loadindex = layer.load(1, {
                                shade: [0.1, '#000']
                            });
                            os.ajax('api/article/delete/', { parm: str }, function (res) {
                                layer.close(loadindex);
                                if (res.statusCode === 200) {
                                    active.reload();
                                    os.success('删除成功！');
                                } else {
                                    os.error(res.message);
                                }
                            });
  ");
            WriteLiteral(@"                      });
                    },
                    toolRecycle: function () {
                        var checkStatus = table.checkStatus('tables')
                            , data = checkStatus.data;
                        if (data.length === 0) {
                            os.error(""请选择要转移的项目~"");
                            return;
                        }
                        var str = '';
                        $.each(data, function (i, item) {
                            str += item.id + "","";
                        });
                        layer.confirm('确定要执行转移到回收站吗？', function (index) {
                            layer.close(index);
                            var loadindex = layer.load(1, {
                                shade: [0.1, '#000']
                            });
                            os.ajax('api/article/recycle/', { parm: str,type:0 }, function (res) {
                                layer.close(loadindex);
                        ");
            WriteLiteral(@"        if (res.statusCode === 200) {
                                    active.reload();
                                    os.success('转移成功！');
                                } else {
                                    os.error(res.message);
                                }
                            });
                        });
                    },
                    toolCopy: function () {
                        isCopy = 1;
                        var checkStatus = table.checkStatus('tables')
                            , data = checkStatus.data;
                        if (data.length === 0) {
                            os.error(""请选择要操作的项目~"");
                            return;
                        }
                        $.each(data, function (i, item) {
                            selStr += item.id + "","";
                        });
                        layer.open({
                            type: 1,
                            title: '批量复制',
            ");
            WriteLiteral(@"                shadeClose: true,
                            shade: 0.2,
                            area: ['650px', '500px'],
                            content: $('.copy-wall').html(),
                            zIndex: ""10000"",
                            success: function () {
                                form.render();
                                $('.btn-open-close').click(function () {
                                    os.closeOpen();
                                });
                            },
                            end: function () {
                                selStr = '';
                            }
                        });
                    },
                    tooltransfer: function () {
                        isCopy = 2;
                        var checkStatus = table.checkStatus('tables')
                            , data = checkStatus.data;
                        if (data.length === 0) {
                            os.error(""请选择要操作的项目~");
            WriteLiteral(@""");
                            return;
                        }
                        $.each(data, function (i, item) {
                            selStr += item.id + "","";
                        });
                        layer.open({
                            type: 1,
                            title: '批量转移',
                            shadeClose: true,
                            shade: 0.2,
                            area: ['650px', '500px'],
                            content: $('.copy-wall').html(),
                            zIndex: ""10000"",
                            success: function () {
                                form.render();
                                $('.btn-open-close').click(function () {
                                    os.closeOpen();
                                });
                            },
                            end: function () {
                                selStr = '';
                            }
                     ");
            WriteLiteral(@"   });
                    }
                };
                table.on('toolbar(tool)', function (obj) {
                    active[obj.event] ? active[obj.event].call(this) : '';
                });
                $('.list-search .layui-btn').on('click', function () {
                    var type = $(this).data('type');
                    active[type] ? active[type].call(this) : '';
                });
                //监听指定开关
                form.on('switch(switch)', function (data) {
                    type = this.checked ? 0 : 1;
                });
                form.on('submit(submit)', function (data) {
                    if (selStr === '') {
                        os.error('请选择要操作的项！');
                        return false;
                    }
                    os.log(data.field);
                    layer.load(1, {
                        shade: [0.1, '#000']
                    });
                    os.ajax('api/article/copyortransfer/', { parm: selStr, type: is");
            WriteLiteral(@"Copy, column: data.field.columnId }, function (res) {
                        os.closeOpen();
                        if (res.statusCode === 200) {
                            active.reload();
                            os.success('操作成功！');
                        } else {
                            os.error(res.message);
                        }
                    });
                    return false;
                });
            });
        function attrHtml(e) {
            var h = '';
            if (e.isTop) {
                h += '<span class=""layui-badge layui-bg-cyan"">推荐</span>';
            }
            if (e.isHot) {
                h += '<span class=""layui-badge layui-bg-cyan"">热点</span>';
            }
            if (e.isScroll) {
                h += '<span class=""layui-badge layui-bg-cyan"">滚动</span>';
            }
            if (e.isSlide) {
                h += '<span class=""layui-badge layui-bg-cyan"">幻灯</span>';
            }
            if (e.isComment) {
");
            WriteLiteral(@"                h += '<span class=""layui-badge layui-bg-cyan"">评论</span>';
            }
            if (e.isWap) {
                h += '<span class=""layui-badge layui-bg-cyan"">移动端</span>';
            }
            return h;
        }
    </script>
</div>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<FytSoa.Web.Pages.FytAdmin.Cms.ArticleModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<FytSoa.Web.Pages.FytAdmin.Cms.ArticleModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<FytSoa.Web.Pages.FytAdmin.Cms.ArticleModel>)PageContext?.ViewData;
        public FytSoa.Web.Pages.FytAdmin.Cms.ArticleModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
